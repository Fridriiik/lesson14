#include <iostream>
#include <string>

int main() {
	std::string WoopWoop;

	std::cout << "Enter the string - ";
	std::getline(std::cin, WoopWoop);

	if (WoopWoop.length() <= 0) {
		WoopWoop = "Do a barrel roll";
	}

	std::cout << "String - " << WoopWoop << "\n";
	std::cout << "Length - " << WoopWoop.length() << "\n";
	std::cout << "First symbol - " << WoopWoop[0] << "\n";
	std::cout << "Last symbol - " <<  WoopWoop[WoopWoop.length() - 1] << "\n";

	return 0; 
}
